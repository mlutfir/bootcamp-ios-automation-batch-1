package page_object;


import static utils.Utils.env;

public class LoginPage extends BasePageObjects {

    public void clickNextTrackApp() {
        tap("BUTTON_NEXT_PERMISSION");
    }

    public void isEntryPointLogin() {
        assertIsDisplayed("BUTTON_WELCOME_LOGIN_PAGE");
    }

    public void clickEntryPointLogin() {
        tap("BUTTON_WELCOME_LOGIN_PAGE");
    }

    public void inputFieldUsername(String text) {
        typeOn("FIELD_USERNAME_LOGIN", env(text));
    }

    public void inputFieldPassword(String password) {
        typeOn("FIELD_PASSWORD_LOGIN", env(password));
    }

    public void tapLoginButton() {
        tap("BUTTON_LOGIN");
    }

    public void isWatchlistPage() {
        assertIsDisplayed("POPUP_SMART_LOGIN_WATCHLIST");
    }
}
