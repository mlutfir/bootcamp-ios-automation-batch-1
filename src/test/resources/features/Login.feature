@iOS @LoginFeature

  Feature: [iOS] Login feature

    @iOS @SuccessLogin
    Scenario: [iOS] User success login
      Given User click next button track app
      When User see entry point login
      And User click entry point login
      And User input username as "USERNAME_SOCIAL"
      And User input password as "PASSWORD_SOCIAL"
      And User click button login
      Then User see watchlist page