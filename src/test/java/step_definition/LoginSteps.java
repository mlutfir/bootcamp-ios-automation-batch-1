package step_definition;

import io.cucumber.java8.En;
import page_object.LoginPage;

public class LoginSteps implements En {

    LoginPage loginPage = new LoginPage();
    public LoginSteps() {
        Given("^User click next button track app$", () -> loginPage.clickNextTrackApp());

        When("^User see entry point login$", () -> loginPage.isEntryPointLogin());

        And("^User click entry point login$", () -> loginPage.clickEntryPointLogin());

        And("^User input username as \"([^\"]*)\"$", (String text) -> loginPage.inputFieldUsername(text));

        And("^User input password as \"([^\"]*)\"$", (String text) -> loginPage.inputFieldPassword(text));

        And("^User click button login$", () -> loginPage.tapLoginButton());

        Then("^User see watchlist page$", () -> loginPage.isWatchlistPage());
    }
}
